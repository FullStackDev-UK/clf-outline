function listenForClick(e) {
    let caller = ( e.target.id == '' && e.target.name === undefined ) ? e.target.parentNode.name : e.target.id;
    let val = e.currentTarget.value;

    switch (caller) {
        case 'elementOne': // E.g. 'contactFormSubmitBtn'
            result.innerHTML += "\nHandle click event for " + caller + " with a current value of " + val;
            break;

        case 'elementTwo':
            result.innerHTML += "\nHandle click event for " + caller + " with a current value of " + val;
            break;

        case 'elementThree':
            result.innerHTML += "\nHandle click event for " + caller + " with a current value of " + val;
            break;
    
        default:
            result.innerHTML += "\nCould not ascertain caller ID or name on clicked element '" + caller + "', or there might just be no handler case defined...";
            break;
    }
}
function listenForChange(e) {
    let caller = ( e.target.id == '' && e.target.name === undefined ) ? e.target.parentNode.name : e.target.id;
    let val = e.currentTarget.value;

    switch (caller) {
        case 'elementTwo':
            result.innerHTML += "\nHandle change event for " + caller + " with a current value of " + val;
            break;

        case 'elementThree':
            result.innerHTML += "\nHandle change event for " + caller + " with a current value of " + val;
            break;

        default:
            result.innerHTML += "\nCould not ascertain caller ID or name on changed element '" + caller + "', or there might just be no handler case defined...";
            break;
    }
}
function listenForBlur(e) {
    let caller = ( e.target.id == '' && e.target.name === undefined ) ? e.target.parentNode.name : e.target.id;
    let val = e.currentTarget.value;
    
    switch (caller) {
        case 'elementThree':
            result.innerHTML += "\nHandle blur event for " + caller + " with a current value of " + val;
            break;
    
        default:
            result.innerHTML += "\nCould not ascertain caller ID or name on blurred element '" + caller + "', or there might just be no handler case defined...";
            break;
    }
}
function listenForKeyUp(e) {
    let caller = ( e.target.id == '' && e.target.name === undefined ) ? e.target.parentNode.name : e.target.id;
    let val = e.currentTarget.value;
    
    switch (caller) {
        case 'elementThree':
            result.innerHTML += "\nHandle keyup event for " + caller + " with a current value of " + val;
            break;

        default:
            result.innerHTML += "\nCould not ascertain caller ID or name on key-up'ed element '" + caller + "', or there might just be no handler case defined...";
            break;
    }
}

let result = document.getElementById('result');

let clickables = document.getElementsByClassName("listenForClick");
for (let i = 0; i < clickables.length; i++) {
    clickables[i].addEventListener("click", listenForClick, false);
}
let changables = document.getElementsByClassName("listenForChange");
for (let i = 0; i < changables.length; i++) {
    changables[i].addEventListener("change", listenForChange, false);
}
let blurrables = document.getElementsByClassName("listenForBlur");
for (let i = 0; i < blurrables.length; i++) {
    blurrables[i].addEventListener("blur", listenForBlur, false);
}
let keyupables = document.getElementsByClassName("listenForKeyUp");
for (let i = 0; i < keyupables.length; i++) {
    keyupables[i].addEventListener("keyup", listenForKeyUp, false);
}
