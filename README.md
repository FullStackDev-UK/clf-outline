# CLF Outline

A CLF, or 'Class-Listener-Function', approach to HTML event handling with JavaScript.

So far, all of the JavaScript in my new development can be boiled down to four classes, four listeners and four functions.

Select case blocks within the functions handle the specific cases using element IDs. The four classes are 'listenForClick', 'listenForChange', 'listenForKeyUp' and 'listenForBlur'. 

I'm sure things will get more complicated as I go along, but at the moment it seems to be working quite well so I thought I'd share.
